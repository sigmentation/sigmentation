# Sigmentation Tool

*Univariate Data Segmentation using Genetic Algorithm*

## Package Contents

This package contains the following commands:

### `sigmentation`

Performs the segmentation process.
Take a look at `sigmentation --help` to get a list of all available options.


### `nparray2json`

Converts numpy files to json arrays.
Take a look at `nparray2json --help` to get a list of all available options.


## Setup

You can install the latest released version by executing
```
pip install sigmentation
```
You can also install the latest version available in the repository by executing
```
pip install git+https://gitlab.com/sigmentation/sigmentation@master
```
