# Changelog

## v0.1.2

  * Fixed tracker and source links
  * Fixed long description

## v0.1.1

  * Improved package meta information
  * added CHANGELOG.md file
  * added README.md file

## v0.1.0

  * Created PyPI package
